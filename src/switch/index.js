import { Redirect, Route, Switch } from "react-router-dom";

import ScrollAnimation from "react-animate-on-scroll";

import BestProductSection from "sections/BestProductSection";
import BestProductDetail from "components/BestProductDetail";

import AllProductPage from "pages/AllProductPage";
import AllProductDetail from "components/AllProductDetail";

// Homepage
import JumboSection from "sections/JumboSection";
import LeadSection from "sections/LeadSection";
import CategoriesSection from "sections/CategoriesSection";
import CleanCodeSection from "sections/CleanCodeSection";
import VideoSection from "sections/VideoSection";
import InstagramSection from "sections/InstagramSection";
import MailListSection from "sections/MailListSection";

import Footer from "components/Footer";

// Pages
import AboutPage from "pages/AboutPage";
import ShippingPage from "pages/ShippingPage";
import TermsPage from "pages/TermsPage";
import FandQPage from "pages/FandQPage";
import ContactPage from "pages/ContactPage";

import NotFoundPage from "pages/NotFoundPage";

const CustomSwitch = () => (
  <>
    <Switch>
      <Route exact path="/assignment-bewdy-organics/">
        <ScrollAnimation animateIn="fadeIn">
          <JumboSection />
          <LeadSection />
          <BestProductSection />
          <CleanCodeSection />
          <CategoriesSection />
          <VideoSection />
          <MailListSection />
          <InstagramSection />
        </ScrollAnimation>
      </Route>

      <Route path="/assignment-bewdy-organics/about">
        <div style={{ paddingTop: "5rem" }}>
          <AboutPage />
        </div>
      </Route>

      <Route path="/assignment-bewdy-organics/fandq">
        <div style={{ paddingTop: "6rem" }}>
          <FandQPage />
        </div>
      </Route>

      <Route path="/assignment-bewdy-organics/terms">
        <div style={{ paddingTop: "7rem" }}>
          <TermsPage />
        </div>
      </Route>

      <Route path="/assignment-bewdy-organics/contact">
        <div style={{ paddingTop: "6rem" }}>
          <ContactPage />
        </div>
      </Route>

      <Route path="/assignment-bewdy-organics/shipping">
        <div style={{ paddingTop: "7rem" }}>
          <ShippingPage />
        </div>
      </Route>

      {/* AllItems Routes */}
      <Route path="/assignment-bewdy-organics/allproducts">
        <div style={{ paddingTop: "6rem" }}>
          <AllProductPage />
        </div>
      </Route>
      <Route path="/assignment-bewdy-organics/allproductdetail/:aid">
        <div style={{ paddingTop: "8rem" }}>
          <AllProductDetail />
        </div>
      </Route>

      {/* Best Product Routes */}
      <Route path="/assignment-bewdy-organics/bestproducts">
        <div style={{ paddingTop: "6rem" }}>
          <BestProductSection />
        </div>
      </Route>
      <Route path="/assignment-bewdy-organics/bestproductdetail/:nid">
        <div style={{ paddingTop: "8rem" }}>
          <BestProductDetail />
        </div>
      </Route>

      <Route exact path="/assignment-bewdy-organics/notfound">
        <div style={{ paddingTop: "9rem" }}>
          <NotFoundPage />
        </div>
      </Route>
      <Route path="/assignment-bewdy-organics">
        <Redirect to="/notfound" />
      </Route>
    </Switch>
    <Footer />
  </>
);

export default CustomSwitch;
