import { createSlice } from "@reduxjs/toolkit";
// import { allItemsState } from "./state";

export const shoppingCartSlice = createSlice({
  name: "shoppingCart",
  initialState: { value: [] },
  reducers: {
    add: (state, action) => {
      // push - arrayの最後に要素を追加する
      state.value.push(action.payload);
    },
  },
});

export const { add } = shoppingCartSlice.actions;

export default shoppingCartSlice.reducer;
