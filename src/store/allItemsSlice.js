import { createSlice } from "@reduxjs/toolkit";
import { allItemsState } from "./state";

export const allItemsSlice = createSlice({
  name: "allItems",
  initialState: { value: allItemsState },
  reducers: {},
});

export const { add, remove } = allItemsSlice.actions;

export default allItemsSlice.reducer;
