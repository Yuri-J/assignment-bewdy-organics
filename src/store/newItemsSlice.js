import { createSlice } from "@reduxjs/toolkit";
import { newItemsState } from "./state";

export const newItemsSlice = createSlice({
  name: "newItems",
  initialState: { value: newItemsState },
  reducers: {
    add: (state, action) => {
      return state.map((item) => {
        if (item.id !== action.payload.id) {
          return item;
        }
        return {
          ...item,
          added: true,
        };
      });
    },
    remove: (state, action) => {
      return state.map((item) => {
        if (item.id !== action.payload.id) {
          return item;
        }
        return {
          ...item,
          added: false,
        };
      });
    },
  },
});

export const { add, remove } = newItemsSlice.actions;

export default newItemsSlice.reducer;
