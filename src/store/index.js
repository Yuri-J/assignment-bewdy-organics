import { configureStore } from "@reduxjs/toolkit";
import allItemsReducer from "./allItemsSlice";
import newItemsReducer from "./newItemsSlice";

import shoppingCartReducer from "./shoppingCartSlice";

export default configureStore({
  reducer: {
    allItems: allItemsReducer,
    newItems: newItemsReducer,
    shoppingCart: shoppingCartReducer,
  },
});
