import { React, useEffect } from "react";
import _ from "lodash";

import { useSelector } from "react-redux";

import styled from "styled-components";
import { Link, useParams } from "react-router-dom";

import { MDBContainer, MDBRow, MDBCol, MDBBtn } from "mdbreact";

const MDBBtnS = styled(MDBBtn)`
  color: white;
  background-color: #607d8b;
  border: 2px solid #607d8b;
  &:hover {
    color: white;
    background-color: #485e69;
    border: 2px solid #485e69;
  }
`;

const BestProductDetail = () => {
  const newItems1 = useSelector((state) => state.newItems.value.newItems1);
  const newItems2 = useSelector((state) => state.newItems.value.newItems2);

  const params = useParams();
  const nid = _.get(params, "nid");

  const newItems = _.concat(newItems1, newItems2);
  const item = _.find(newItems, { _id: nid });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <>
      <MDBContainer>
        <MDBRow>
          <MDBCol lg="6">
            <img
              src={item.img}
              className="img-fluid"
              alt=""
              style={{ width: "26rem" }}
            />
          </MDBCol>
          <MDBCol lg="6" className="mx-auto">
            <h1 className="pb-5">{item.title}</h1>
            <h5 style={{ lineHeight: "2rem" }}>{item.desc}</h5>
            <MDBRow className="py-5">
              <MDBCol>{item.title}</MDBCol>
              <MDBCol>
                {item.size} / {item.price}
              </MDBCol>
            </MDBRow>

            <MDBRow>
              <MDBCol>
                <MDBBtnS color="" className="w-100">
                  Add to Cart
                </MDBBtnS>
              </MDBCol>
            </MDBRow>
          </MDBCol>
        </MDBRow>
        <div className="text-center py-5">
          <Link to="/assignment-bewdy-organics/">
            <MDBBtn outline color="blue-grey">
              Back to Home
            </MDBBtn>
          </Link>
        </div>
      </MDBContainer>
    </>
  );
};

export default BestProductDetail;
