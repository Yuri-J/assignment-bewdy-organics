import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import _ from "lodash";

import { slide as Menu } from "react-burger-menu";

import { MDBBtn } from "mdbreact";
import styled from "styled-components";

import BagSVG from "assets/bag.svg";

// function handleClick(e) {
//   e.preventDefault();
//   console.log("You clicked this.");
// }

const MDBBtnS = styled(MDBBtn)`
  color: white;
  background-color: #607d8b;
  border: 2px solid #607d8b;
  &:hover {
    color: white;
    background-color: #485e69;
    border: 2px solid #485e69;
  }
`;

const SideBar = (props) => {
  const shoppingCart = useSelector((state) => state.shoppingCart.value);
  const dedupedById = _.countBy(shoppingCart, (item) => {
    return item._id;
  });
  const dedupedShoppingCart = _.map(Object.keys(dedupedById), (id) => {
    const itemObject = _.find(shoppingCart, { _id: id });
    const amount = dedupedById[id];
    return { ...itemObject, amount };
  });

  return (
    <>
      <Menu
        {...props}
        customBurgerIcon={<img src={BagSVG} alt="bagIcon" />}
        right
      >
        <h2>BAG</h2>

        <h4 className="font-weight-bold">Total: $ {shoppingCart.length}.00</h4>

        <hr style={{ backgroundColor: "grey" }} />
        {_.map(dedupedShoppingCart, (item, index) => {
          const { title, price, amount } = item; // Destructuring
          return (
            <div key={index}>
              <p className="font-weight-bold">{title}</p>
              <p>
                Qty:{amount} / Price: {price}
              </p>
              <hr />
            </div>
          );
        })}
        <Link to="/">
          <MDBBtnS color="" className="w-100">
            CHECKOUT
          </MDBBtnS>
        </Link>
      </Menu>
    </>
  );
};

export default SideBar;
