import React from "react";
import { Link } from "react-router-dom";

import { MDBCardImage, MDBView } from "mdbreact";

const AllProduct = (props) => {
  const detailsPath = `/assignment-bewdy-organics/allproductdetail/${props._id}`;

  return (
    <>
      <div>
        <Link to={detailsPath}>
          <MDBView hover zoom>
            <MDBCardImage
              src={props.img[0]}
              alt=""
              className="mx-auto img-fluid w-100"
              waves
            />
          </MDBView>
        </Link>
        <div className="py-3">
          <p className="font-weight-bold">{props.title}</p>
          <p>
            {props.size} / {props.price}
          </p>
        </div>
      </div>
    </>
  );
};

export default AllProduct;
