import { BrowserRouter as Router } from "react-router-dom";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import SideBar from "./SideBar";

import styled from "styled-components";

import {
  MDBContainer,
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBNavbarToggler,
  MDBCollapse,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
} from "mdbreact";

import navLogoSVG from "assets/nav-logo.svg";

const MDBNavbarNavS = styled(MDBNavbarNav)`
  letter-spacing: 0.2em;
  @media (min-width: 992px) {
    padding: 0 12rem;
  }
`;

const MDBNavLinkS = styled(MDBNavLink)`
  @media (max-width: 992px) {
    display: none;
  }
`;

const NavBar = (props) => {
  const { children } = props;

  const [isOpen, setIsOpen] = useState(false);
  const toggleCollapse = () => setIsOpen(!isOpen);

  const shoppingCart = useSelector((state) => state.shoppingCart.value);

  return (
    <>
      <Router>
        <MDBNavbar
          color=""
          style={{ backgroundColor: "#c9abab" }}
          fixed="top"
          dark
          expand="md"
          scrolling
        >
          <MDBNavbarToggler onClick={toggleCollapse} />
          <MDBCollapse id="navbarCollapse3" isOpen={isOpen} navbar>
            <MDBNavbarNavS className="flex-center" right>
              <MDBNavItem className="mr-3">
                <MDBNavLink to="/assignment-bewdy-organics/allproducts">
                  HAIR
                </MDBNavLink>
              </MDBNavItem>
              <MDBNavItem className="mr-3">
                <MDBNavLink to="/assignment-bewdy-organics/allproducts">
                  SKIN
                </MDBNavLink>
              </MDBNavItem>
              <MDBNavItem className="mr-3">
                <MDBNavLink to="/assignment-bewdy-organics/allproducts">
                  BODY
                </MDBNavLink>
              </MDBNavItem>

              <MDBNavbarBrand>
                <MDBNavLinkS to="/assignment-bewdy-organics/">
                  <img src={navLogoSVG} className="img-fluid" alt="" />
                </MDBNavLinkS>
              </MDBNavbarBrand>

              <MDBNavItem>
                <MDBNavLink to="/assignment-bewdy-organics/about">
                  ABOUT
                </MDBNavLink>
              </MDBNavItem>
              <MDBNavItem>
                <MDBDropdown>
                  <MDBDropdownToggle nav caret>
                    <div className="d-none d-md-inline">SHIPPING</div>
                  </MDBDropdownToggle>
                  <MDBDropdownMenu className="dropdown-default">
                    <MDBNavLink to="/assignment-bewdy-organics/shipping">
                      <MDBDropdownItem tag="span">DELIVELY</MDBDropdownItem>
                    </MDBNavLink>
                    <MDBNavLink to="/assignment-bewdy-organics/fandq">
                      <MDBDropdownItem tag="span">FAQs</MDBDropdownItem>
                    </MDBNavLink>
                    <MDBNavLink to="/assignment-bewdy-organics/terms">
                      <MDBDropdownItem tag="span">
                        TERMS & CONDITIONS
                      </MDBDropdownItem>
                    </MDBNavLink>
                  </MDBDropdownMenu>
                </MDBDropdown>
              </MDBNavItem>
              <MDBNavItem>
                <MDBNavLink to="/assignment-bewdy-organics/contact">
                  CONTACT
                </MDBNavLink>
              </MDBNavItem>
            </MDBNavbarNavS>

            <MDBNavbarNav right>
              <MDBNavItem>
                <MDBNavLink className="waves-effect waves-light" to="#!">
                  <SideBar pfageWrapId={"page-wrap"} />
                </MDBNavLink>
              </MDBNavItem>

              <MDBNavItem>
                <MDBNavLink
                  disabled
                  className="waves-effect waves-light"
                  to="#!"
                >
                  <h5>{shoppingCart.length}</h5>
                </MDBNavLink>
              </MDBNavItem>
            </MDBNavbarNav>
          </MDBCollapse>
        </MDBNavbar>
        <MDBContainer
          fluid
          className="mx-0 px-0"
          // style={{ paddingTop: isNavbarFixed ? 102 : 0 }}
        >
          {children}
        </MDBContainer>
      </Router>
    </>
  );
};

const navBarWithSwitch = (CustomSwitch) => (
  <>
    <NavBar>
      <CustomSwitch />
    </NavBar>
  </>
);

export default navBarWithSwitch;
