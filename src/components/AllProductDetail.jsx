import { React, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link, useParams } from "react-router-dom";
import _ from "lodash";

import styled from "styled-components";
import "react-responsive-carousel/lib/styles/carousel.min.css";

import { add } from "../store/shoppingCartSlice";

import { Carousel } from "react-responsive-carousel";

import { MDBContainer, MDBRow, MDBCol, MDBBtn } from "mdbreact";

const MDBBtnS = styled(MDBBtn)`
  color: white;
  background-color: #607d8b;
  border: 2px solid #607d8b;
  &:hover {
    color: white;
    background-color: #485e69;
    border: 2px solid #485e69;
  }
`;

const AllProductDetail = () => {
  const allItems = useSelector((state) => state.allItems.value);

  const params = useParams();
  const aid = _.get(params, "aid");
  const item = _.find(allItems, { _id: aid });

  const dispatch = useDispatch();

  const [buttonText, setButtonText] = useState("Add to Cart");

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <MDBContainer>
        <MDBRow>
          <MDBCol lg="6">
            <Carousel className="p-5">
              {_.map(item.img, (url, index) => (
                <div key={index}>
                  <img src={url} className="img-fluid" alt="" />
                </div>
              ))}
            </Carousel>
          </MDBCol>
          <MDBCol lg="6" className="pt-3">
            <h1 className="pt-4">{item.title}</h1>
            <h5 className="text-muted py-2">Size:{item.size}</h5>
            <p>{item.desc}</p>
            <MDBRow className="py-4">
              <MDBCol>
                <h4 className="">{item.price}.00</h4>
              </MDBCol>
            </MDBRow>
            <MDBRow>
              <MDBCol>
                <MDBBtnS
                  onClick={() => {
                    setButtonText("✔ Item Added to Cart");
                    dispatch(add(item));
                  }}
                  color=""
                  className="w-100"
                >
                  {buttonText}
                </MDBBtnS>
              </MDBCol>
            </MDBRow>
            <div>
              <h5 className="pt-5">Usage</h5>
              <hr />
              <p>{item.usage}</p>

              <h5 className="pt-3">Ingredients</h5>
              <hr />
              <p>{item.ingredients}</p>
            </div>
          </MDBCol>
        </MDBRow>
        <div className="text-center py-5">
          <Link to="/assignment-bewdy-organics/">
            <MDBBtn outline color="blue-grey">
              Back to Home
            </MDBBtn>
          </Link>
        </div>
      </MDBContainer>
    </>
  );
};

export default AllProductDetail;
