import { React, useEffect } from "react";
import { Link } from "react-router-dom";
import _ from "lodash";

import { useSelector } from "react-redux";

import AllProduct from "../components/AllProduct";

import { MDBContainer, MDBRow, MDBCol, MDBBtn } from "mdbreact";

const AllProductPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const allItems = useSelector((state) => state.allItems.value);

  return (
    <>
      <MDBContainer>
        <div>
          <Link to="/" className="text-left" style={{ color: "#908585" }}>
            <p>
              <span>HOME</span>&nbsp;/&nbsp;<span>ALL ITEMS</span>
            </p>
          </Link>

          <h3 className="h2-responsive pt-5">All Items</h3>
          <hr style={{ height: "2rem" }} />
          <MDBRow className="text-center">
            {_.map(allItems, (a) => (
              <MDBCol key={a._id} lg={3} md={6}>
                <AllProduct key={a._id} {...a} />
              </MDBCol>
            ))}
          </MDBRow>
          <div className="py-5 text-center">
            <Link to="/assignment-bewdy-organics/">
              <MDBBtn outline color="blue-grey">
                Back to Home
              </MDBBtn>
            </Link>
          </div>
        </div>
      </MDBContainer>
    </>
  );
};

export default AllProductPage;
