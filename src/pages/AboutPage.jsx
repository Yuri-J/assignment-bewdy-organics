import { React, useEffect } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBMask, MDBView } from "mdbreact";

import ScrollAnimation from "react-animate-on-scroll";
import { Parallax } from "react-parallax";

import Title4PNG from "assets/bg-title4.png";
import LeftPNG from "assets/left.png";
import RightPNG from "assets/right.png";
import OrganicPNG from "assets/organic.png";

import IconVSVG from "assets/icon-v.svg";
import IconCSVG from "assets/icon-c.svg";
import IconOSVG from "assets/icon-o.svg";
import IconGSVG from "assets/icon-g.svg";

const AboutPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <ScrollAnimation animateIn="fadeIn" animateOnce={true}>
        <MDBContainer className="p-0" fluid>
          <MDBView>
            <img src={Title4PNG} className="img-fluid w-100" alt="" />
            <MDBMask className="flex-center" overlay="black-light">
              <h1 className="font-weight-bold text-white">OUR STORY</h1>
            </MDBMask>
          </MDBView>
          <MDBContainer>
            <h2 className="font-weight-bold text-center pt-5">About Us</h2>
            <p className="py-5">
               Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta nesciunt debitis corrupti commodi consequatur! Iusto doloremque alias quibusdam a placeat? Eveniet exercitationem vel esse fuga dolorem, minima culpa optio consequuntur. Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic adipisci inventore at architecto reprehenderit ullam corporis rerum facilis eos quibusdam. Minima voluptate impedit hic dignissimos exercitationem magnam fugiat harum at?
            </p>

            <MDBRow>
              <MDBCol
                md="4"
                className="d-flex flex-column justify-content-center"
              >
                <h2 className="font-weight-bold">Science</h2>
                <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta nesciunt debitis corrupti commodi consequatur! Iusto doloremque alias quibusdam a placeat? Eveniet exercitationem vel esse fuga dolorem, minima culpa optio consequuntur. Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic adipisci inventore at architecto reprehenderit ullam corporis rerum facilis eos quibusdam. Minima voluptate impedit hic dignissimos exercitationem magnam fugiat harum at?
                </p>
              </MDBCol>

              <MDBCol size="6" md="8">
                <img src={LeftPNG} alt="" className="img-fluid w-100" />
              </MDBCol>
            </MDBRow>

            <MDBRow className="py-5">
              <MDBCol size="6" md="8">
                <img src={RightPNG} alt="" className="img-fluid w-100" />
              </MDBCol>
              <MDBCol
                md="4"
                className="d-flex flex-column justify-content-center"
              >
                <h2 className="font-weight-bold">
                  Sustainability And The Planet
                </h2>
                <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta nesciunt debitis corrupti commodi consequatur! Iusto doloremque alias quibusdam a placeat? Eveniet exercitationem vel esse fuga dolorem, minima culpa optio consequuntur. Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic adipisci inventore at architecto reprehenderit ullam corporis rerum facilis eos quibusdam. Minima voluptate impedit hic dignissimos exercitationem magnam fugiat harum at?
                </p>
              </MDBCol>
            </MDBRow>
            <MDBRow className="text-center m-5">
              <MDBCol>
                <img src={IconVSVG} className="img-fluid w-50" alt="" />
                <h5 className="py-4 font-weight-bold">VEGAN</h5>
              </MDBCol>

              <MDBCol>
                <img src={IconCSVG} className="img-fluid w-50" alt="" />
                <h5 className="py-4 font-weight-bold">CRUELTY FREE</h5>
              </MDBCol>
              <MDBCol>
                <img src={IconOSVG} className="img-fluid w-50" alt="" />
                <h5 className="py-4 font-weight-bold">100% ORGANIC</h5>
              </MDBCol>
              <MDBCol>
                <img src={IconGSVG} className="img-fluid w-50" alt="" />
                <h5 className="py-4 font-weight-bold">GMO FREE</h5>
              </MDBCol>
            </MDBRow>
          </MDBContainer>

          <Parallax
            blur={0}
            bgImage={OrganicPNG}
            bgImageAlt="powder"
            strength={240}
            className="p-5"
          >
            <MDBContainer className="p-5">
              <h4 className="m-5 text-white" style={{ lineHeight: "2rem" }}>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta nesciunt debitis corrupti commodi consequatur! Iusto doloremque alias quibusdam a placeat? Eveniet exercitationem vel esse fuga dolorem.
              </h4>
            </MDBContainer>
          </Parallax>
        </MDBContainer>
      </ScrollAnimation>
    </>
  );
};

export default AboutPage;
