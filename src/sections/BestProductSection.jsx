import React from "react";
import _ from "lodash";

import { useSelector } from "react-redux";
import BestProduct from "../components/BestProduct";

import { Container, Carousel, Row, Col } from "react-bootstrap";
import ScrollAnimation from "react-animate-on-scroll";

const BestProductSection = () => {
  const newItems1 = useSelector((state) => state.newItems.value.newItems1);
  const newItems2 = useSelector((state) => state.newItems.value.newItems2);

  return (
    <>
      <Container className="text-center">
        <ScrollAnimation animateIn="fadeIn" animateOnce={true}>
          <h5 className="pb-2">The Best in Bewdy</h5>
          <h2 className="responsive-h1 py-3 font-weight-bold">
            OUR BEST SELLERS
          </h2>
        </ScrollAnimation>
        <ScrollAnimation animateIn="fadeIn" animateOnce={true}>
          <Carousel className="p-5" controls={false} interval={2500}>
            <Carousel.Item>
              <Row>
                {_.map(newItems1, (n) => (
                  <Col key={n._id} className="pb-3 mx-auto">
                    <BestProduct key={n._id} {...n} />
                  </Col>
                ))}
              </Row>
            </Carousel.Item>
            <Carousel.Item>
              <Row>
                {_.map(newItems2, (n) => (
                  <Col key={n._id} className="pb-3 mx-auto">
                    <BestProduct key={n._id} {...n} />
                  </Col>
                ))}
              </Row>
            </Carousel.Item>
          </Carousel>
        </ScrollAnimation>
      </Container>
    </>
  );
};

export default BestProductSection;
