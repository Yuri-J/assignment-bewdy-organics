import React from "react";
import { MDBContainer } from "mdbreact";

import ScrollAnimation from "react-animate-on-scroll";

import ReactPlayer from "react-player";

import VideoMP4 from "assets/video.mp4";

const VideoSection = () => {
  return (
    <>
      <ScrollAnimation animateIn="fadeIn" animateOnce={true}>
        <MDBContainer fluid className="p-0">
          <div className="py-3">
            <ReactPlayer
              className="react-player"
              url={VideoMP4}
              width="100%"
              height="100%"
              controls={false}
              muted={true}
              playing={true}
              loop={true}
            />
          </div>
        </MDBContainer>
      </ScrollAnimation>
    </>
  );
};

export default VideoSection;
